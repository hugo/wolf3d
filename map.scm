(define-module (map)
  :use-module (ice-9 rdelim)
  :export ())

(define-public (read-map port)
  (let loop ((board '()))
    (let ((p (read-line port 'split)))
      (let ((line (car p))
            (delim (cdr p)))
        (if (eof-object? delim)
            (reverse board)
            (loop (cons line board)))))))

(define-public (parse-map spec)
  (let ((arr (make-array #f (length spec) (string-length (car spec))))
        (spawn '(1 1)))
    (array-index-map! arr
                      (lambda (i j)
                        (case (string-ref (list-ref spec i) j)
                          ((#\P #\p) (set! spawn (list j i)) 'space)
                          ((#\space) 'space)
                          ((#\L) 'lamp)
                          ((#\#) 'wall)
                          ((#\:) 'grass)
                          ((#\;) 'teleporter)
                          ((#\*) 'window)
                          ((#\%) 'entrance))))
    (list spawn arr)))

(define-public board-spawn car)
(define-public board-data cadr)
(define-public (board-height board) (car (array-dimensions (board-data board))))
(define-public (board-width board) (cadr (array-dimensions (board-data board))))

#;
(define game-map
  (parse-map
   '(
     "  #    #######****#**#**#"
     "###%####:::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::#   #::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::#   #::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::#   #::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "  #  ###**#*#**#**#**#**#"
     "  ## #####################")))

#;

(define game-map
  (parse-map
   '(
     "  ######                 "
     "  #  % #                 "
     "  #  ####################"
     "  #    %                #"
     "  #    #############%#%##"
     "  #    #          #  #  #"
     "  #    %          #  #  #"
     "  #    #######****#**#**#"
     "###%####:::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::#   #::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::#   #::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::#   #::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "::*   *::::::::::::::::::"
     "  #  ###**#*#**#**#**#**#"
     "  #  % #  # #  #  #  #  #"
     "  #  ###  # #  #  #  #  #"
     "  #  ####%#%##%##%#%##%##"
     "  #  %                   "
     "  ## #####################")))




(define-module (util)
  :export (*unspecified* signum cross-product pi tau int)
  :replace (set!)
  :use-module (srfi srfi-1)
  )

(define-public *unspecified* ((@ (guile) if) #f #f))

(define-syntax set!
  (syntax-rules (=)
    ((_) *unspecified*)

    ((_ field = (op args ...) rest ...)
     (begin ((@ (guile) set!) field (op field args ...))
            (set! rest ...)))

    ((_ field proc)
     ((@ (guile) set!) field proc))

    ((_ field proc rest ...)
     (begin ((@ (guile) set!) field proc) (set! rest ...)))))

(define (signum x)
  (cond ((zero? x) 0)
        ((positive? x) 1)
        (else -1)))

(define-public sgn signum)

(define (cross-product l1 l2)
  (concatenate
   (map (lambda (a)
          (map (lambda (b) (list a b))
               l2))
        l1)))

(define-public (decimals x)
  (- x (truncate x)))

(define pi 3.141592653589793)
(define tau (* 2 pi))

(define int (compose inexact->exact truncate))

(define-public (cot a)
  (/ (tan a)))

(define-module (draw)
  :use-module (util)
  :use-module (sdl2 render)
  :use-module (sdl2 rect)
  :replace (fill-rect)
  :export (set-draw-color)
)


(define-public current-renderer (make-parameter #f))
(define-public current-tile-size (make-parameter 50))

(define-public (draw-line x1 y1 x2 y2)
  (render-draw-line
   (current-renderer)
   (int (* (current-tile-size) x1)) (int (* (current-tile-size) y1))
   (int (* (current-tile-size) x2)) (int (* (current-tile-size) y2))))

(define-public (fill-rect x y w h)
 (render-fill-rect
  (current-renderer)
  (make-rect (int (* (current-tile-size) x))
             (int (* (current-tile-size) y))
             (int (* (current-tile-size) w))
             (int (* (current-tile-size) h)))))

(define-public (draw-line x1 y1 x2 y2)
  (render-draw-line
   (current-renderer)
   (int (* x1 (current-tile-size))) (int (* y1 (current-tile-size)))
   (int (* x2 (current-tile-size))) (int (* y2 (current-tile-size)))))

(define-public (make-rect* x y w h)
  (make-rect (int (* x (current-tile-size)))
             (int (* y (current-tile-size)))
             (int (* w (current-tile-size)))
             (int (* h (current-tile-size)))))

(define-public (fill-rects rects)
  (render-fill-rects (current-renderer) rects))

(define* (set-draw-color r g b #:optional (a #xFF))
  (set-render-draw-color (current-renderer) r g b a))

(define-public (clear)
  (clear-renderer (current-renderer)))

(define-public (present)
  (present-renderer (current-renderer)))


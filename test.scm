(load "main.scm")

(for-each
 (lambda (x y)
   (let ((ray-count 8))
     (for-each
      (lambda (theta)
        (let ((a (* tau theta)))
          (find-next-wall
           x y a
           (lambda (nx ny)
             (format #t "a = ~a τ (~,1f, ~,1f) => (~,1f, ~,1f), + Δ(~,1f, ~,1f)~%"
              theta x y nx ny (- nx x) (- ny y))))))
      (iota ray-count 0 1/8)))
   (newline))
 '(1.1 1.1 1.9 1.9)
 '(1.1 1.9 1.1 1.9))

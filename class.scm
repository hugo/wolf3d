(define-module (class)
  :use-module (oop goops)
  :use-module (util)
  :export (vector-length v3 make-player make-ray))

(define-class <v3> ()
  (x #:accessor x #:init-keyword #:x)
  (y #:accessor y #:init-keyword #:y)
  (z #:accessor z #:init-keyword #:z))
(export x y z)

(define-method (write (v <v3>) port)
  (write (list 'v3 (x v) (y v) (z v))
         port)
  )

(define* (v3 #:optional (x 0) (y 0) (z 0))
  (make <v3> #:x x #:y y #:z z))

(define-method (+ (v <v3>) (u <v3>))
  (v3 (+ (x v) (x u))
      (+ (y v) (y u))
      (+ (z v) (z u))))

(define-method (* (a <number>) (v <v3>))
  (v3 (* a (x v))
      (* a (y v))
      (* a (z v))))

(define (vector-length v)
  (sqrt (+ (* (x v) (x v))
           (* (y v) (y v))
           (* (z v) (z v)))))

(define-class <player> ()
  (pos #:accessor p
       #:init-form (v3 1 1)
       #:init-keyword #:p)
  (angle #:accessor a #:init-value 0 #:init-keyword #:a)
  (feild-of-view #:accessor fov #:init-value (/ tau 8)))
(export p a fov)

(define (make-player x y a)
   (make <player> #:p (v3 x y) #:a a))


(define-class <ray> ()
  (angle #:accessor a #:init-keyword #:a)
  (length #:accessor length #:init-keyword #:length #:init-value #f)
  (vec #:accessor vec #:init-keyword #:v)
  (pos-on-wall #:accessor pos-on-wall)
  (hit-point #:getter hitf #:init-keyword #:hitf)
  (hit-float-point #:getter hitf #:init-keyword #:hitf)
  (solid? #:getter solid? #:init-keyword #:solid?)
  (tile-type #:getter type
             #:init-value #f
             #:init-keyword #:type)
  (hits-to-wall #:getter hits-to-wall
                #:init-value -1
                #:init-keyword #:htw)
  )

(export a length vec pos-on-wall hitf solid? type hits-to-wall)

(define-method (initialize (self <ray>) args)
  (next-method)
  (unless (length self)
    (set! (length self) (vector-length (vec self))))

  (let* ((xx (x (hitf self)))
         (yy (y (hitf self))))
    (set! (pos-on-wall self)
          (min
           (max (abs (decimals xx))
                (abs (decimals yy)))
           (max (- 1 (abs (decimals xx)))
                (- 1 (abs (decimals yy)))))

          ))

  )

(define (make-ray . args)
  (apply make <ray> args))
